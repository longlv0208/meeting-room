import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Login from "../views/Login.vue";

import { Guard, useGuard } from "../composables/useGuard";

export const routes = {
  login: {
    path: "/",
    name: "login",
    component: Login,
    meta: {
      guard: Guard.NOT_AUTH,
    },
  },
  dashboard: {
    path: "/dashboard",
    name: "dashboard",
    component: Dashboard,
    meta: {
      guard: Guard.AUTH,
    },
  },
  room: {
    path: "/room",
    name: "room",
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/AboutView.vue"),
  },
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: Object.values(routes),
});

router.beforeEach(async (to, from, next) => {
  const { meta } = to;
  console.log(" meta.guard", meta.guard);

  const guard = meta.guard as Guard;
  if (!guard) next();
  else {
    const guardResult = useGuard(guard);
    if (guardResult) {
      next(guardResult);
    } else next();
  }
});

export default router;
