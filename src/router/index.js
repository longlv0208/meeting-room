import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Login from "../views/Login.vue";

import { Guard, useGuard } from "../composables/useGuard";

export const routes = {
  login: {
    path: "/",
    name: "login",
    component: Login,
    meta: {
      guard: Guard.NOT_AUTH,
    },
  },
  dashboard: {
    path: "/dashboard",
    name: "dashboard",
    component: Dashboard,
  },
  room: {
    path: "/room",
    name: "room",
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/AboutView.vue"),
  },
  report: {
    path: "/report",
    name: "report",
    component: () => import("../views/ReportView.vue"),
  },
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: Object.values(routes),
});

export default router;
