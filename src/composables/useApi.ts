import axios, { AxiosError, Method } from "axios";
import { message } from "ant-design-vue";
import { HTTP_CODE, NETWORK_ERROR_CODE } from "../utils/constant";
import { isRef, ref, unref, watchEffect } from "vue";

axios.defaults.baseURL = import.meta.env.VITE_APP_API_URL;
axios.defaults.timeout = 30000;

console.log("axios.defaults.baseURL", import.meta.env.VITE_APP_API_URL)

type ApiResponse<T> = {
  code: string;
  message: string;
  items: T | undefined;
};
const defaultHeader = {
  "Content-Type": "application/json",
  Authorization:
    "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbmdsb2JhbEBnbWFpbC5jb20iLCJ1c2VySWQiOjEsInVzZXJOYW1lIjoiQWRtaW5pc3RyYXRvciIsInBob25lTnVtYmVyIjoiMDkwMzUzNzU1MiIsInJvbGVzIjoiUk9MRV9BRE1JTiIsImlhdCI6MTY2NTQ3MTIxOCwiZXhwIjoxNjY1NDg1NjE4fQ.kqXKHcsRxB7aNpH4p7X_wIcCCJ9OPPy03uyaZ_hddK3mrjvPLISn25-Y3ICwDYevM28M_wXW_OS9o2sm-hLDVg",
};

export async function useApi<T>(config: {
  url: any;
  method: Method;
  body?: any;
  params?: any;
  headers?: any;
}) {
  const { url, method, body, params, headers = defaultHeader } = config;
  const data = ref();
  const error = ref();

  async function doApi() {
    data.value = null;
    error.value = null;

    const urlValue = unref(url);

    try {
      switch (method) {
        case "GET": {
          const res = await axios.get<ApiResponse<T>>(urlValue, {
            params,
            headers,
          });
          data.value = res.data;
          break;
        }
        case "POST": {
          const res = await axios.post<ApiResponse<T>>(urlValue, body, {
            params,
            headers,
          });
          data.value = res.data;
          break;
        }
        case "PATCH": {
          const res = await axios.patch<ApiResponse<T>>(urlValue, body, {
            params,
            headers,
          });
          data.value = res.data;
          break;
        }
        default:
          break;
      }
    } catch (e: any | AxiosError) {
      if (axios.isAxiosError(e)) {
        error.value = (e.response as any)?.data?.message || true;
        if (e.response?.status === HTTP_CODE.BAD_REQUEST) {
          return;
        }

        // Internal server error response
        if (e.response?.status === HTTP_CODE.INTERNAL_SERVER_ERROR) {
          message.error(error.value);
        }
        if (e.code === NETWORK_ERROR_CODE) {
          message.error("NETWORK_ERROR");
        }
      } else {
        error.value = e.message;
        // Check if its a network error
        if (e.code === NETWORK_ERROR_CODE) {
          message.error("INTERNAL_SERVER_ERROR");
        }
      }
    }
  }

  if (isRef(url)) {
    await watchEffect(doApi);
  } else {
    await doApi();
  }

  return { data, error, retry: doApi };
}
