import { routes } from "../router";
// import store from "../stores";

export enum Guard {
  AUTH = "auth",
  NOT_AUTH = "not_auth",
}
export function useGuard(guard: Guard) {
  const accessToken = localStorage.getItem("access-token");
  switch (guard) {
    case Guard.NOT_AUTH:
      if (!accessToken) {
        return null;
      }
      return { name: routes.dashboard.name };
    case Guard.AUTH:
      if (accessToken) {
        return null;
      }
      return { name: routes.login.name };
    default:
      return { name: routes.login.name };
  }
}
