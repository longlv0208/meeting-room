import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

import "./assets/main.css";
import "./assets/tailwind.css";
import "ant-design-vue/dist/antd.css";
import Antd from "ant-design-vue";
import { createI18n } from "vue-i18n";

const messages = {
  en: {
    message: { hello: "{msg} world" },
  },
  ja: {
    message: {
      hello: "こんにちは、世界",
    },
  },
};

// 2. Create i18n instance with options
const i18n = createI18n({
  locale: "en", // set locale
  fallbackLocale: "en", // set fallback locale
  messages, // set locale messages
  // If you need to specify other options, you can set other options
  // ...
});

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(Antd);

app.use(i18n);
app.mount("#app");
