export const NETWORK_ERROR_CODE = 'ERR_NETWORK';
export enum HTTP_CODE {
  BAD_REQUEST = 400,
  INTERNAL_SERVER_ERROR = 500,
}

export const MONTHS_IN_YEAR = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]
export const HOURS_IN_DAY =  [
  '8h',
  '9h',
  '10h',
  '11h',
  '12h',
  '13h',
  '14h',
  '15h',
  '16h',
  '17h',
  '18h',
]