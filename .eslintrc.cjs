/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-prettier",
    "airbnb-base",
  ],
  parserOptions: {
    ecmaVersion: "latest",
    parser: "@typescript-eslint/parser",
    sourceType: "module",
  },
  plugins: ["vue", "@typescript-eslint"],
  rules: {
    "import/no-unresolved": "off",
    "no-shadow": "off",
    "no-param-reassign": "off",
    "vue/no-multiple-template-root": "off",
    "import/prefer-default-export": "off",
    "linebreak-style": "off",
    "import/extensions": "off",
  },
};
